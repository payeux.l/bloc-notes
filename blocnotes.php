<!-------------- PAGE INDEX ------------>

<!DOCTYPE html>
<html lang="fr">
<?php // AFFICHER ERREUR PHP SUR SITE
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
?>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="title" content="Bloc-Notes PHP."/>
        <meta property="og:type" content="BlocNotes" />
        <meta name="author" content="Lilian PAYEUX" />
        <title>Bloc-Notes PHP | Lilian Payeux</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="" /> <!-- CHANGER LE LOGO PAR BLOC NOTES --->
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts        
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />-->

        <!-- Core theme CSS (includes Bootstrap)
        <link href="" rel="stylesheet" />-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
      <div class="d-flex justify-content-center border-bottom">
        <h1>Bloc-Notes en PHP</h1>
      </div>
        <h4><a href="https://lildev.fr">Revenir sur Lildev.fr</a></h4>

        <?php 
        include('connexionDB.php');

        if(!empty($_POST)) { //Si la variable post n'est pas vide alors on traite
            $valide=true;
            extract($_POST);

            if(isset($_POST['submit'])) { // Si l'utilisateur valide (submit) alors on récupère la valeur dans une variable
                $note = htmlspecialchars($note);
            }

            if (empty($note)) { // Si Form vide alors on dit erreur
                $valide = false;
                echo ('note vide');
            }

            elseif ($valide = true) { // Sinon on traite et enregistre dans BDD
                $date_creation_note = date('Y-m-d H:i:s');
                $DB->insert("INSERT INTO blocnotes (note, date_creation_note) VALUES (?,?)", array($note, $date_creation_note));
                header("blocnotes.php");
                exit;
            }
            

        }

        //Affichage des valeurs de la BDD
        $afficher_notes = $DB->query("SELECT *
            FROM blocnotes");


        ?>
        

        <div class="d-flex justify-content-center">
        <form action="" method="post">
          <div class="input-group mb-3">
            <input type="text" name="note" placeholder="Ajouter une note" class="form-control">
            <div class="input-group-append">
            <input class="btn btn-outline-secondary" type="submit" name="submit" value="Ajouter">
          </div>
        </div>
        </form>
        </div>
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Notes</th>
                  <th scope="col">Date</th>
                  <th scope="col">Supprimer</th>
                  
                </tr>
              </thead>  
            <?php
            while($afficher_note = $afficher_notes->fetch())
            {
            
            echo '
            
              <tr>
                  <td>'.  $afficher_note['id'] . '</td>
                  <td>'. $afficher_note['note'] . '</td>
                  <td>'. $afficher_note['date_creation_note'].'</td>
                  <td>
                    <a href="deletenote.php?id='. $afficher_note['id'].'">
                        <i class="far fa-trash-alt"></i>
                    </a>
                  </td>
              </tr>
            

              
           ';
            
            }

            $afficher_notes->closeCursor();//Termine traitement de la requete
            ?>
    </table>
    <span class="border-top-0"></span>
    <footer>
      Copyright Lilian Payeux. <a href="https://lildev.fr">Lildev.fr</a>
    </footer>
    </body>
    </html>
